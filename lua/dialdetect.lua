--- 模块功能：播放拨号音功能
-- @author miuser
-- @module elderphone.dialdetect
-- @license MIT
-- @copyright miuser
-- @release 2020-05-15

require"pins"
require"common"
module(...,package.seeall)

--拨号号码
local dialNum=""
--开始拨号检测
isDetecting=0
--脉冲计数
local paulseCount=-1

--中断方式下,上一次跳变到高电平的tick计数，高电平脉冲的持续时间interval
local highTick,highInterval=0,0
--中断方式下,上一次跳变到低电平的tick计数，低电平脉冲的持续时间interval
local lowTick,lowInterval=0,0

--轮询方式下高电平初始计时，保持高电平的时间
local highTick_DC,highInterval_DC=0,0
--轮询方式下低电平初始计时，保持低电平的时间
local lowTick_DC,lowInterval_DC=0,0

function gpio15IntFnc(msg)
    if isDetecting==1 then 
        --log.info("testGpioSingle.gpio15IntFnc",msg,getGpio15Fnc())
        if msg==cpu.INT_GPIO_POSEDGE then
            --上升沿中断
            --计算下降沿脉宽
            lowInterval=rtos.tick()-lowTick
            highTick=rtos.tick()
            --log.info("elderphone.dialdetect","lowInterval is "..lowInterval)
        else
            --下降沿中断
            highInterval=rtos.tick()-highTick
            lowTick=rtos.tick()
            --log.info("elderphone.dialdetect","highInterval is "..highInterval)
            if highInterval>10 and highInterval<30 then
                paulseCount=paulseCount+1
            end
        end
    end
end

 --GPIO15配置为中断，可通过getGpio15Fnc()获取输入电平，产生中断时，自动执行gpio15IntFnc函数
getGpio15Fnc = pins.setup(15,gpio15IntFnc)


local function dialout()
    sys.publish("DIALOUT",dialNum)
    sys.publish("UARTOUT","正在拨号:"..dialNum)
    dialNum=""
end

sys.taskInit(function()
    while true do
        --log.info("elderphone.dialdetect","highInterval_DC is"..highInterval_DC)
        --当前拨号线为低电平
        if getGpio15Fnc()==0 then
            highTick_DC=0
            highInterval_DC=0
            if (lowTick_DC==0) then lowTick_DC=rtos.tick() end
            lowInterval_DC=rtos.tick()-lowTick_DC
        else
        --当前拨号线为高电平
            lowTick_DC=0
            lowInterval_DC=0
            if (highTick_DC==0) then highTick_DC=rtos.tick() end
            highInterval_DC=rtos.tick()-highTick_DC
        end

        --低电平持续0.2S，则判断未开始拨号
        if lowInterval_DC>40 and isDetecting==0 then
            log.info("lowInterval_DC=",lowInterval_DC)
            paulseCount=0
            log.info("elderphone.dialdetect","preparing dialing")
            sys.publish("PLATE_MOVING")
            sys.publish("UARTOUT","拨号盘转动")
            isDetecting=1

        end
        --持续高电平>0.3S,且有脉冲计数，则输出脉冲计数
        if highInterval_DC>60 and paulseCount~=-1 then
            sys.publish("PLATE_STOP")
            sys.publish("UARTOUT","拨号盘停止")
            sys.publish("DIALING",paulseCount)
            if (paulseCount==0) then
                --拨出号码
                dialout()
            else
                log.info("elderphone.dialdetect","dialing# "..(paulseCount%10))
                sys.publish("UARTOUT","拨号"..(paulseCount%10))
                dialNum=dialNum..(paulseCount%10)
                log.info("elderphone.dialdetect","dialNum:",dialNum)
                --sys.publish("UARTOUT","当前号码:"..dialNum)
                --满足11位自动播出
                if (#dialNum==11) then dialout() end
            end
            paulseCount=-1
            isDetecting=0
        end
        sys.wait(20)
    end
end
)

local function clrcallingnum()
    dialNum=""
end
sys.subscribe("ON_HOOK", clrcallingnum)
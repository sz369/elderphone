--- 模块功能：播放拨号音功能
-- @author miuser
-- @module elderphone.tone
-- @license MIT
-- @copyright miuser
-- @release 2020-05-15



require"audio"
require"common"
module(...,package.seeall)


local function pub(s)
    sys.publish(s)
end

local function uar(s)
    sys.publish("UARTOUT",s)
end


PWRON,CALL,SMS,TTS,REC = 4,3,2,1,0

--播放音频文件测试接口，每次打开一行代码进行测试
local function Play450HzTone()
    audio.play(0,"FILE","/lua/450Hz.mp3",audiocore.VOL3,nil,true)
    pub("PLAY_450Hz OK")
    uar("播放450Hz拨号音")
end
sys.subscribe("PLAY_450Hz", Play450HzTone)

local function Stop450HzTone()
    audio.stop()
    pub("STOP_450Hz OK")
    uar("停止播放拨号音")
end
sys.subscribe("STOP_450Hz", Stop450HzTone)



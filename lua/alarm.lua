--- 模块功能：GPIO功能测试.
-- @author miuser
-- @module elderphone.alarm
-- @license MIT
-- @copyright miuser
-- @release 2020-05-15

require"pins"
module(...,package.seeall)


local function pub(s)
    sys.publish(s)
end

local function uar(s)
    sys.publish("UARTOUT",s)
end


-- pmd.ldoset(2,pmd.LDO_VSIM1) -- GPIO 29、30、31
-- pmd.ldoset(2,pmd.LDO_VLCD) -- GPIO 0、1、2、3、4、6
-- pmd.ldoset(2,pmd.LDO_VMMC) -- GPIO 24、25、26、27、28

--振铃的状态
local alarm_ON=0

--正在振铃
local alarming=0

--振铃IO状态
local level = 0
--振铃间隔
local pause = 0
--GPIO18 振铃脉冲 GPIO19 线路切换
local setGpio18Fnc = pins.setup(pio.P0_18,0)
local setGpio19Fnc = pins.setup(pio.P0_19,0)

sys.taskInit(function()
        while true do
            if alarm_ON==1 and pause<150 then
                level = level==0 and 1 or 0
                setGpio18Fnc(level)
                --log.info("Swtitch level of GPIO 18",level)
                if (alarming==0) then
                    pub("AMARMING")
                    alarming=1
                    uar("打铃")
                end
            else
                setGpio18Fnc(0)
                if (alarming==1) then
                    pub("AMARMING STOP")
                    alarming=0
                    uar("振铃停止")
                end
            end
            --检查振铃间隔
            pause=pause+1
            --(0-150)*20ms 响铃 ，150-200，暂停
            if pause==250 then pause=0 end
            sys.wait(20)
        end
end
)

local function Alarm_ON()
    alarm_ON=1
    setGpio19Fnc(1)
    log.info("elderphone.alarm","alarm on")
    pub("ALARM_ON OK")

end

sys.subscribe("ALARM_ON", Alarm_ON)


local function ALARM_OFF()
    alarm_ON=0
    setGpio19Fnc(0)
    log.info("elderphone.alarm","alarm off")
    pub("ALARM_OFF OK")
end
sys.subscribe("ALARM_OFF", ALARM_OFF)

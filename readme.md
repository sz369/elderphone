# ElderPhone 工作原理说明书 V0.1

## 基本功能

Elderphone固件是用来驱动老式拨号盘电话机用来实现电话的基本功能。 基本工作工作原理如下：

1）电话两端通过 DC-DC 升压模块提供10V直流电，经过1/20分压，进行线路电压检测，当电话挂机状态，检测电压约为500mV，此时线路电压约为10V。 摘机后电压下降为约3V，此时检测电压为150mV。检测端口为ADC2

2）当拨号盘转动时，周期性短接线路两侧，产生拨号脉冲被GPIO9检测，当号码满11位，或用半拨号方式手动结束，拨打电话。

3）当外部电话接入，铃音电路开启，GPIO19设置为高电平切换线路升压变压器初级到25Hz信号发生器，同时GPIO18输出25Hz方波脉冲驱动产生振铃信号。

## 目录组成部分

### 组装示意图 assembly

电话可以在闲鱼上购得同款

![](./readme/0.jpg)


系统由软硬件两部分组成

### 硬件部分 baseboard&coreboard



![](./readme/6.jpg)

电话核心板在淘宝搜索Air724电话核心板购买，购买链接：
https://item.taobao.com/item.htm?spm=a230r.1.14.16.7842a199WLhFUA&id=624610770134&ns=1&abbucket=10#detail
![](./readme/1.JPG)
底板部分图纸完全开源，暂无电路板销售，本项目中提供图纸的源文件
![](./readme/5.jpg)

![](./readme/2.jpg)

![](./readme/3.jpg)

### 软件部分 lua

采用lua编程，源码开源，MIT授权欢迎取用


## 基本原理

1 基于Elderphone硬件，各个模块完成指定的功能。 

2 通过Luatask消息机制完成模块间通讯

3 各个模块间无代码级依赖关系，可以独立运行，无公有变量

4 模块间互相可以通过消息通讯进行协作

## 代码运行逻辑（状态机）

1）监测联网状态，如果收到底层发布的联网成功消息，则电话状态为[已联网]。

2）如果联网后摘机，则电话状态转化为[拨号检测]

3) 如果收到底层发布的电话挂断消息，当前状态为[响铃] (响铃但未接听电话),状态切换为[扣机],否则(当前状态为摘机），播放语音“通话已结束。

4) 如果检测到摘机消息，则 如果当前为[响铃]，则状态改为[通话]，如果状态为[拨号检测]且已经联网,则播放450Hz音频，未联网则播放语音“正在初始化”

5）如果收到要拨号盘发出的拨打电话消息，则关闭振铃，拨打电话

6）如果收到挂机消息，且当前状态为[通话]，则挂断电话，状态转化为扣机，停止播放450Hz音频

7）如果来电终止，则振铃关闭

8）当电话状态机发生变化，从串口打印当前状态

## 模块消息订阅

sys.publish(...)发布消息

sys.subscribe(id, callback) 订阅消息

sys.publish("UARTOUT",msg) 发布串口调试消息

## 例子

1）打开振铃

require "Alarm"

sys.publish("ALARM_ON)

2）拨打电话

require "Call"

sys.publish("CALL",117) 打号码为117的电话


## 模块消息定义：

## Alarm

### 接收消息

"ALARM_ON" 打铃

"ALARM_OFF" 关闭铃声

### 发布消息

"AMARM_ON OK" 打铃成功

"AMARM_OFF OK" 铃声关闭成功

"AMARMING" 正在振铃

"AMARMING STOP" 振铃停止

### 串口调试消息

"打铃"
"振铃停止"


## Call 模块

### 接收消息

"CALL",s_TEL 打号码为s_TEL的电话

"OVER" 挂断电话

"ANSWER" 接听当前来电


### 发布消息

"CALL OK",s_TEL 拨打s_TEL的电话成功

"OVER OK" 挂断电话成功

"ANSWER OK" 接听当前电话成功

### 串口调试信息

"已联网"

"电话接通"

"通话断开"

"有来电，号码为"..num


## dialdetect 模块

### 发布消息

"PLATE_MOVING" 拨号盘在移动

"PLATE_STOP" 拨号盘停止

"DIALING"，paulseCount 拨打脉冲计数

"DIALOUT", dialNum 拨出号码


### 串口调试消息

"正在拨号:"..dialNum  

"拨号盘转动" 

"拨号盘停止"

"拨号"..paulse 

## hookdetect 模块

### 发布消息

"OFF_HOOK" 摘机

"ON_HOOK" 挂机

### 串口调试消息

"摘机"

"挂机"

## tone 模块

### 接收消息

"PLAY_450Hz" 播放450Hz拨号音

"STOP_450Hz" 停止播放拨号音


### 发布消息

"PLAY_450Hz OK" 播放450Hz拨号音成功

"STOP_450Hz OK" 停止播放拨号音成功

### 串口调试消息

"播放450Hz拨号音"

"停止播放拨号音"

## uartcomm 模块

### 接收消息（目前映射硬件串口1，可以改为USB虚拟串口及其他硬件串口）

"UARTOUT", uartrsv 从串口输出调试信息

### 发布消息

msg,[arg1],[arg2] 发布任意系统消息，并可包含最多两个参数

## system 模块

### 接收消息

"NET_STATE_REGISTERED" 网络已连接

"CALL_INCOMING",incoming 有来电

"OFF_HOOK" 摘机

"ON_HOOK" 挂机

"CALL_DISCONNECTED" 通话挂断

"DIALOUT",num 拨打号码num

"PLATE_MOVING" 拨号盘移动

### 发布消息

"PLAY_450Hz" 播放拨号音

"ALARM_ON" 打铃

"ANSWER" 接听电话

"ALARM_OFF" 关闭铃音

"ANSWER" 接听电话

"CALL",num 拨打号码

"OVER" 扣机

"STOP_450Hz" 停止播放拨号音

